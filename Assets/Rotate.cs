﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    public float Speed = 10f;

    private Vector3 touchPos1;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
	    if (Input.GetMouseButtonDown(0))
	    {
	        touchPos1 = Input.mousePosition;
	    }
	    if (Input.GetMouseButton(0))
	    {
	        var pos = Input.mousePosition;
	        var dir = (pos - touchPos1).normalized;
	        dir = dir * Speed * Time.deltaTime;
            transform.Rotate(dir);

	        touchPos1 = pos;
//
//            var rX = Input.GetAxis("Mouse X") * Speed * Mathf.Deg2Rad * Time.deltaTime;
//            var rY = Input.GetAxis("Mouse Y") * Speed * Mathf.Deg2Rad * Time.deltaTime;
//
//            transform.Rotate(Vector3.up, -rX);
//            transform.Rotate(Vector3.right, -rY);
	    }
	}
    
}
