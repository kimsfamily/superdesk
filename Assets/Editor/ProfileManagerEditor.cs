﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Assertions.Comparers;
using Object = UnityEngine.Object;

public class ProfileManagerEditor : EditorWindow
{
    private ProfileManager _target;
    private Profile _profileTemplate;
    private Object _profileObjectTemplate;
    private static ProfileManagerEditor _window;
    private Profile _selectProfile;
    private bool _isViewSize = true;
    private bool _isViewPosition = false;
    bool _isViewOnlySelect = false;

    [MenuItem("Family/SuperDesk")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        _window = (ProfileManagerEditor)EditorWindow.GetWindow(typeof(ProfileManagerEditor));
        _window.Show();
    }
    
    public bool IsSizing = true;

    ProfileManager FindProfileManager(GameObject searchObject)
    {
        if (searchObject == null) return null;

        ProfileManager target = null;

        target = searchObject.GetComponent<ProfileManager>();
        
        return target;
    }

    void Update()
    {
//        if (_window == null)
//            _window = (ProfileManagerEditor)EditorWindow.GetWindow(typeof(ProfileManagerEditor));
//        EditorUtility.SetDirty(_window);
        Repaint();
    }
    void OnSelectionChange()
    {
        if(_window == null)
            _window = (ProfileManagerEditor)EditorWindow.GetWindow(typeof(ProfileManagerEditor));


        var selectObject = Selection.activeGameObject;

        if (_target == null || _target.gameObject != selectObject)
        {
            var tProfileManager = FindProfileManager(selectObject);
            if (tProfileManager)
            {
                _target = tProfileManager;
            }
        }
        var profile = selectObject.GetComponent<Profile>();

        if (profile == null && selectObject.transform.parent != null)
        {
            var tt = selectObject.transform.parent.gameObject;

            profile = tt.GetComponent<Profile>();

            if(profile)
                Selection.activeObject = tt.gameObject;
        }

        if (profile)
        {
            _selectProfile = profile;

            _target = profile.transform.parent.GetComponent<ProfileManager>();

            if (_target == null && profile.transform.parent)
            {
                _target = profile.transform.parent.parent.GetComponent<ProfileManager>();
            }
        }

        
        EditorUtility.SetDirty(_window);
        Repaint();
    }
    public void OnInspectorGUI()
    {
        //update and redraw:
        if (GUI.changed)
        {
            EditorUtility.SetDirty(_window);
        }
    }
    public void OnGUI()
    {
        if (_target == null) return;

        _target.LoadProfiles();

        _profileTemplate = EditorGUILayout.ObjectField("Template", _profileTemplate, typeof(Profile), false) as Profile;
        
        if (_profileTemplate)
        {
            if (_profileTemplate != null)
            {
                _profileTemplate.Target.transform.localScale = EditorGUILayout.Vector3Field("Default Size", _profileTemplate.Target.transform.localScale);
            }
        }
        
        if (GUILayout.Button("Load Profiles"))
        {
            _target.LoadProfiles();
        }
        EditorGUILayout.BeginHorizontal();

        _isViewSize = EditorGUILayout.Toggle("Size", _isViewSize);
        _isViewPosition = EditorGUILayout.Toggle("Position", _isViewPosition);
        _isViewOnlySelect = EditorGUILayout.Toggle("Selected", _isViewOnlySelect);

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField(_target.name + (IsSizing ? " Size Editor" : " Position Editor"));
        if (_profileTemplate && GUILayout.Button("Create Profile"))
        {
            _target.CreateProfile(_profileTemplate);
        }
        EditorGUILayout.EndHorizontal();

        if (_isViewOnlySelect)
        {
            var profile = _selectProfile;

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(profile.name + "-Size", GUILayout.Width(100));
            GUI.color = Color.white;
            profile.Size = IntVector3ToVector3(EditorGUILayout.Vector3IntField("", Vector3ToIntVector3(profile.Size) * 10)) / 10f;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(profile.name + "-Posi", GUILayout.Width(100));
            GUI.color = Color.white;
            profile.transform.localPosition = WorldPos(IntVector3ToVector3(EditorGUILayout.Vector3IntField("", Vector3ToIntVector3(WorkPos(profile.transform.localPosition)) * 10)) / 10f);
            EditorGUILayout.EndHorizontal();

            return;
        }

        var isRed = false;

        foreach (var profile in _target.Profiles)
        {
            
            if (_selectProfile && profile.gameObject == _selectProfile.gameObject)
            {
                isRed = true;
            }
            else
            {
                isRed = false;
            }
            
            
            if (_isViewSize)
            {
                EditorGUILayout.BeginHorizontal();
                if (isRed) GUI.color = Color.red;
                EditorGUILayout.LabelField(profile.name + "-Size", GUILayout.Width(100));
                GUI.color = Color.white;
                profile.Size = IntVector3ToVector3(EditorGUILayout.Vector3IntField("", Vector3ToIntVector3(profile.Size) * 10)) / 10f;
                EditorGUILayout.EndHorizontal();
            }
            
            if (_isViewPosition)
            {
                EditorGUILayout.BeginHorizontal();
                if (isRed) GUI.color = Color.red;
                EditorGUILayout.LabelField(profile.name + "-Posi", GUILayout.Width(100));
                GUI.color = Color.white;
                profile.transform.localPosition = WorldPos(IntVector3ToVector3(EditorGUILayout.Vector3IntField("",Vector3ToIntVector3(WorkPos(profile.transform.localPosition)) * 10))/10f);
                EditorGUILayout.EndHorizontal();
            }
            
            var text = profile.TextMesh;
            if (text != null)
            {
                var temp = profile.Size;
                text.transform.position = profile.transform.position + new Vector3(temp.x, temp.y * 1.5f, -temp.z)/2f;

                temp *= 10f;
                var w = (int)Mathf.Max(Mathf.Max(temp.x,temp.y), temp.z);
                var w2 = (int)GetWidth(temp, WorldPos(profile.transform.localPosition * 10f));

                //if(w != w2) text.text = w2 + "(" + w + ")mm"; 
                //else

                    text.text = w + "mm";

                profile.LookCamera(SceneView.lastActiveSceneView.camera);
            }
        }

        GUI.color = Color.white;
    }

    Vector3 WorldPos(Vector3 vec)
    {
        return new Vector3(vec.x * -1, vec.y, vec.z);
    }
    Vector3 WorkPos(Vector3 vec)
    {
        return new Vector3(vec.x * -1, vec.y, vec.z);
    }
    Vector3 IntVector3ToVector3(Vector3Int vector)
    {
        return new Vector3(vector.x, vector.y, vector.z);
    }
    Vector3Int Vector3ToIntVector3(Vector3 vector)
    {
        return new Vector3Int((int)vector.x, (int)vector.y, (int)vector.z);
    }
    float GetWidth(Vector3 size, Vector3 pos)
    {
        var id = 0;

        var sizes = new float[] {size.x, size.y, size.z};
        var poses = new float[] {pos.x, pos.y, pos.z};

        if (size.y > size.x) id = 1;
        else if (size.z > size.y) id = 2;
        else id = 0;

        return sizes[id] + poses[id];
    }
}
