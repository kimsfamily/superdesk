﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProfileManager : MonoBehaviour
{
    public List<Profile> Profiles = new List<Profile>();

    public void CreateProfile(Profile template)
    {
        var profile = Instantiate(template);
        profile.name = "Profile" + Profiles.Count;
        profile.transform.SetParent(transform);

        profile.transform.localPosition = Vector3.zero;
        profile.Size = Vector3.one * 10;
        Profiles.Add(profile);
    }

    public void LoadProfiles()
    {
        var temp = GetComponentsInChildren<Profile>(true);

        Profiles.Clear();
        foreach (var profile in temp)
        {
            Profiles.Add(profile);
        }
    }
}
