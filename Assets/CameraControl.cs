﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public GameObject Target;

    private Vector3 _prePos;
    public Camera MyCam;
    public float Speed = 10f;

    private float _zoomRate;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetAxis("Mouse ScrollWheel") > 0)
	    {
	        MyCam.orthographicSize -= 10;
	    }

	    if (Input.GetAxis("Mouse ScrollWheel") < 0)
	    {
	        MyCam.orthographicSize += 10;
	    }

        if (Input.touchCount > 1)
	    {
	        var a = Input.touches[0].position;
	        var b = Input.touches[1].position;

	        var c = Vector2.Distance(a, b);

            if(_zoomRate != 0)
	            MyCam.orthographicSize += _zoomRate - c;

	        _zoomRate = c;
	    }
        else if (Input.touchCount > 0 || Input.GetMouseButton(0))
        {
            var pos = Input.mousePosition;
            var dir = (pos - _prePos).normalized;
            dir = dir * Speed * Time.deltaTime;

            transform.LookAt(Target.transform);

            transform.RotateAround(Target.transform.position, Vector3.up, dir.x);
            transform.RotateAround(Target.transform.position, Vector3.left, dir.y);


            _prePos = pos;
        }
        else
        {
            _zoomRate = 0;
        }

	    
    }
}
