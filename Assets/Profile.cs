﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class Profile : MonoBehaviour
{
    public TextMesh TextMesh;

    public Transform Target;
    
    public Vector3 Size
    {
        set
        {
            if (Target == null) return;
            Target.localScale = value;
            Target.transform.localPosition = new Vector3(-value.x, value.y, value.z) / 2f;
        }
        get
        {
            return Target.localScale;
        }
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawCube(transform.position, transform.lossyScale);
    }
    public void LookCamera(Camera camera)
    {
        if (camera == null) return;
        TextMesh.transform.LookAt(camera.transform);
    }
    
    // Update is called once per frame
    void Update()
    {
        LookCamera(Camera.main);
    }
}
